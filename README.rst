==================
django-view-admin
==================

django-view-admin adds a ViewModelAdmin which can be used
in place of the normal ModelAdmin to add view permissions.

It is not compatible with Grappelli.

It has been tested only been tested with Django 1.6


Installation
============

Add 'view_admin' to your INSTALLED_APPS before 'django.contrib.admin', e.g.

	INSTALLED_APPS = (
	    'view_admin',
	    ...
	    'django.contrib.admin',
	)

In your project urls.py, add the following:

	import view_admin as admin
	admin.autodiscover()

In your admin.py files, use ViewModelAdmin instead of ModelAdmin, e.g.

	from view_admin import ViewModelAdmin

	admin.site.register(Poll, ViewModelAdmin)

Run the following to generate the view permissions

	python manage.py syncdb


Please note that this app monkeypatches django.contrib.admin so that autodiscover still works.
from django.contrib import admin
from .models import *
from view_admin import ViewModelAdmin

# Register your models here.
admin.site.register(Poll, ViewModelAdmin)

from django.db import models

# Create your models here.
class Poll(models.Model):
	question = models.CharField(max_length=200)

class Option(models.Model):
	poll = models.ForeignKey(Poll)
	value = models.CharField(max_length=50)
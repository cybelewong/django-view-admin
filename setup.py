from setuptools import setup, find_packages  # Always prefer setuptools over distutils
from codecs import open  # To use a consistent encoding
from os import path
from view_admin import VERSION

long_desc = '''
%s

%s
''' % (open('README.rst').read(), open('CHANGELOG.rst').read())


setup(
    name='django-view-admin',

    # Versions should comply with PEP440.  For a discussion on single-sourcing
    # the version across setup.py and the project code, see
    # http://packaging.python.org/en/latest/tutorial.html#version
    version=VERSION.replace(' ', '-'),

    description='A Django app that adds view permissions to the admin',
    long_description=long_desc,

    # The project's main homepage.
    url='https://bitbucket.org/cybelewong/django-view-admin/',

    # Author details
    author='Cybele Wong',
    author_email='cybele.wong@gmail.com',

    # Choose your license
    license='Freely Distributable',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        'Framework :: Django',

        # Indicate who your project is intended for
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',

        # Pick your license as you wish (should match "license" above)
        'License :: Freely Distributable',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 2.7',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],

    # What does your project relate to?
    keywords='django admin view permission setuptools development',

    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    packages=find_packages(exclude=['demo', 'contrib', 'docs', 'tests*']),

    # List run-time dependencies here.  These will be installed by pip when your
    # project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/technical.html#install-requires-vs-requirements-files
    install_requires=['django'],

    include_package_data=True,

    zip_safe=False,
)

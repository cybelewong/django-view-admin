"""
django-view-admin is an app which adds ViewModelAdmin, a ModelAdmin subclass
which adds view permissions.
"""
VERSION = '0.1.0'

# from .options import ViewModelAdmin, ViewAdminSite


# # code to override admin site + autodiscover: http://nerdydevel.blogspot.com.au/2012/07/customize-django-adminsite.html

# # Let's create AdminSite instance
# # NOTICE: here you can ovverride admin class and create your own AdminSite implementation
# site = ViewAdminSite()

# import django.contrib.admin
# django.contrib.admin.site = site
# # By the way now you can use the standard django admin autodiscover function
# # you can import it here
# from django.contrib.admin import autodiscover